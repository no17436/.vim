
"
" run & configure pathogen
"
call pathogen#infect()

"
" kick-ass defaults
"
set nocompatible  "to get rid of unnecessary vi compatibility
set bs=2          "set backspace to be able to delete previous characters
set number        "enable line numbering, taking up 6 spaces
set rnu           "relative line numbering
let mapleader="," "set leader to ,
set modelines=0   "for security (http://lists.alioth.debian.org/pipermail/pkg-vim-maintainers/2007-June/004020.html)
syntax on
set scrolloff=3   "3 lines of buffer when scrolling
"write regexp without annoying \/
nnoremap / /\v
vnoremap / /\v
set vb t_vb=      "no irritating visual bell

"
" indenting
"
filetype indent on        "indent depends on filetype
filetype plugin indent on "indent based on pathogen?
set tabstop=2             "set tab character to 2 characters
set shiftwidth=2          "set indent width for autoindent
set expandtab             "turn tabs into whitespace
set smartindent           "Turn on smart indent
set wrap!                 "Turn off word wrapping
set textwidth=79
set formatoptions=qrn1
set colorcolumn=85

"
" incremental search
"
set ignorecase "case-insensitive by default... but...
set smartcase  "if you type at least one upper cased letter it becomes case-sensitive
set gdefault   "so you can skip /g in %s/aaa/bbb/g
set incsearch
set showmatch
set hlsearch
"hide search highlight with ,<space>
nnoremap <leader><space> :noh<cr>

"
" document navigation
"

"go to opening/closing bracket with tab
nnoremap <tab> %
vnoremap <tab> %

"Shortcuts to auto hard-indent entire file (like cleanup in tidy)
nmap <F11> 1G=G
imap <F11> <ESC>1G=Ga

" Informative status line
" set statusline=%F%m%r%h%w\ [TYPE=%Y\ %{&ff}]\ [%l/%L\ (%p%%)]

"
" solarized color scheme (http://ethanschoonover.com/solarized)
"
set t_Co=16
let g:solarized_termcolors=16
let g:solarized_termtrans=1
set background=light
colorscheme solarized
set cursorline " set line highlight


"
" Backups & Files (doesn't work on windows)
"
set backup                   " Enable creation of backup file.
set backupdir=~/.vim/backups " Where backups will go.
set directory=~/.vim/tmp     " Where temporary files will go.

" Auto save files when focus is lost
au FocusLost * silent! wa
set autowriteall

"
" UTF
"
scriptencoding utf-8
set encoding=utf-8
set fileencodings=utf-8


"
" key bindings
"

" tabs
map tj :tabnext<CR>
map tk :tabprev<CR>
map th :tabfirst<CR>
map tl :tablast<CR>
map tt :tabedit<Space>
map tn :tabnext<Space>
map tm :tabm<Space>
map tn :tabnew<CR>
map td :tabclose<CR>

"new vertical split with focus
nnoremap <leader>s <C-w>v<C-w>l

"close vertical split
nnoremap <leader>c <C-w>c

"ruby (ruby on rails) bindings
inoremap <C-l> <Space>=><Space>
nnoremap <C-l> Alogger.info "------------------- #{}"<Left><Left>
nnoremap ; :wa<cr>
nnoremap <C-c> I#<Space><ESC>


"
" Ruby & Rails specific
"

"coffeescript foldable after zi
au BufNewFile,BufReadPost *.coffee setl foldmethod=indent nofoldenable

"custom file extensions
au BufNewFile,BufRead *.thor set filetype=ruby

"discard tmp/* files from command-t menu
set wildignore+=tmp/**
set wildignore+=app/assets/images/**
set wildignore+=vendor/assets/images/**


"functions

function! NumberToggle()
  if(&relativenumber == 1)
    set number
  else
    set relativenumber
  endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>

